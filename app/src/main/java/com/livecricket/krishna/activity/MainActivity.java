package com.livecricket.krishna.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.livecricket.krishna.R;
import com.livecricket.krishna.utils.AppConfig;
import com.livecricket.krishna.utils.Constants;
import com.livecricket.krishna.utils.PreferenceHelper;
import com.livecricket.krishna.utils.Progress;
import com.livecricket.krishna.utils.Utils;

import java.util.Random;

import us.zoom.sdk.JoinMeetingParams;
import us.zoom.sdk.MeetingError;
import us.zoom.sdk.MeetingOptions;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.MeetingServiceListener;
import us.zoom.sdk.MeetingStatus;
import us.zoom.sdk.MeetingViewsOptions;
import us.zoom.sdk.ZoomError;
import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKInitializeListener;

public class MainActivity extends AppCompatActivity implements MeetingServiceListener, ZoomSDKInitializeListener {

    private static final String TAG = "MainActivity";
    private static final String ALLOWED_CHARACTERS ="3M_925669522_352_HLerHOOi";

    int STYPE = MeetingService.USER_TYPE_ZOOM; // there is three types of users differs with privileges

    Button instant, custom, join;

    EditText meeting_id, meeting_password;

    private Progress progress;
    private Utils utils;
    private PreferenceHelper helper;
    private String mettingID = "";
    private String username = "";
    private AudioManager am;
    private String zoomkey="";
    private String zoomsequrity="";
    private String meetingPassword="";
    private String randomuser="";
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);



        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        am.setMicrophoneMute(true);
        progress = new Progress(MainActivity.this);
        utils = new Utils(MainActivity.this);
        helper = new PreferenceHelper(MainActivity.this, AppConfig.PREFERENCE.PREF_FILE);


        initZoomSDK();

        meeting_id = (EditText) findViewById(R.id.meetingID);
        meeting_password = (EditText) findViewById(R.id.meetingPassword);

        mettingID = helper.LoadStringPref(AppConfig.PREFERENCE.MEETINGID,"");
        meetingPassword = helper.LoadStringPref(AppConfig.PREFERENCE.MEETINGPWD,"");
        randomuser = helper.LoadStringPref(AppConfig.PREFERENCE.RANDOMUSERSTRING,"");
//        username = helper.LoadStringPref(AppConfig.PREFERENCE.USER_NAME,"");
        Log.e(TAG, "onCreate: "+randomuser );
        username = getRandomString(10);



        Log.e(TAG, "onCreate meeting id : "+mettingID );

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.msg_connect));
        progress.showDialog();
        utils.hideKeyboard();

        instant = (Button) findViewById(R.id.instant);
        instant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });


        custom = (Button) findViewById(R.id.custom);
        custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                /*new GetContacts().execute();*/

//                registerForCallInvites();
//                getRoom();
//                startCustomMeeting(meeting_id.getText().toString().trim());

            }
        });


        join = (Button) findViewById(R.id.join);
        join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                joinMeeting(meeting_id.getText().toString().trim(), meeting_password.getText().toString().trim());

            }
        });


       if(helper.LoadIntPref(AppConfig.PREFERENCE.ZOOM_INTIALIZE,0)==1){
           joinMeeting(mettingID, meetingPassword);
       }



    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        /*startActivity(new Intent(MainActivity.this, HomeActivity.class));
        finish();*/
        recreate();
    }

    private static String getRandomString(final int sizeOfRandomString)
    {
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder(sizeOfRandomString);
        for(int i=0;i<sizeOfRandomString;++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }


    @Override
    public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {

//        Log.e(TAG, "onZoomSDKInitializeResult: " + errorCode + " " + internalErrorCode);
//        Log.d(TAG, "onZoomSDKInitializeResult: " + errorCode + " " + internalErrorCode);
        Log.d(TAG, "onZoomSDKInitializeResult errorCode : " + errorCode );
        Log.d(TAG, "onZoomSDKInitializeResult internalErrorCode : " + internalErrorCode );
        if (errorCode != ZoomError.ZOOM_ERROR_SUCCESS) {
            Toast.makeText(getApplicationContext(),
                    "Failed to initialize Zoom SDK. Error: " + errorCode +
                            ", internalErrorCode=" + internalErrorCode, Toast.LENGTH_LONG);

        } else {
//            Toast.makeText(getApplicationContext(), "Initialize Zoom SDK successfully.", Toast.LENGTH_LONG).show();
            Log.e(TAG, "onZoomSDKInitializeResult: "+"Initialize Zoom SDK successfully." );
            ZoomSDK sdk = ZoomSDK.getInstance();
            MeetingService meetingService = sdk.getMeetingService();
            if (meetingService != null) meetingService.addListener(this);
//            if(mettingID.isEmpty()){
//                Toast.makeText(this, "Please Join again", Toast.LENGTH_SHORT).show();
//                finish();
//                return;
//            }

            helper.initPref();
            helper.SaveIntPref(AppConfig.PREFERENCE.ZOOM_INTIALIZE,1);
            helper.ApplyPref();

            joinMeeting(mettingID, meetingPassword);

        }

    }

    @Override
    public void onZoomAuthIdentityExpired() {

    }


    public void initZoomSDK() {
        ZoomSDK sdk = ZoomSDK.getInstance();

        zoomkey = helper.LoadStringPref(AppConfig.PREFERENCE.ZOOMKEY,"");
        zoomsequrity = helper.LoadStringPref(AppConfig.PREFERENCE.ZOOMSECRET,"");

        if(zoomkey.isEmpty() &&zoomsequrity.isEmpty()){
            Toast.makeText(this, "Please contact to admin.Update the credentials", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!sdk.isInitialized()) {


            sdk.initialize(this, zoomkey, zoomsequrity, Constants.WEB_DOMAIN, this);
//            //set your own keys for dropbox , oneDrive and googleDrive
//            sdk.setDropBoxAppKeyPair(this, null/*DROPBOX_APP_KEY*/, null/*DROPBOX_APP_SECRET*/);
//            sdk.setOneDriveClientId(this, null/*ONEDRIVE_CLIENT_ID*/);
//            sdk.setGoogleDriveClientId(this, null /*GOOGLE_DRIVE_CLIENT_ID*/);
            Log.e(TAG, "initZoomSDK: " + "success");
            Log.e(TAG, "initZoomSDK: " + sdk.getDomain());
        } else {

            Log.e(TAG, "initZoomSDK: " + sdk.getDomain());
            MeetingService meetingService = sdk.getMeetingService();
            if (meetingService != null) meetingService.addListener(this);
        }
    }


    @Override
    protected void onDestroy() {
        ZoomSDK zoomSDK = ZoomSDK.getInstance();

        Log.e(TAG, "onDestroy: " );
        if (zoomSDK.isInitialized()) {
            MeetingService meetingService = zoomSDK.getMeetingService();
            meetingService.removeListener(this);
        }

        super.onDestroy();
    }


//    @Override
//    protected void onStop() {
//        ZoomSDK zoomSDK = ZoomSDK.getInstance();
//
//        Log.e(TAG, "onstop: " );
//        if (zoomSDK.isInitialized()) {
//            MeetingService meetingService = zoomSDK.getMeetingService();
//            meetingService.removeListener(this);
//        }
//        super.onStop();
//
//    }

    public void joinMeeting(String meetingNo, String password) {
        if (meetingNo == null || meetingNo.length() == 0) {
            Toast.makeText(this, "You need to enter a scheduled meeting number.", Toast.LENGTH_LONG).show();
            return;
        }
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        if (!zoomSDK.isInitialized()) {
            Log.e(TAG, "joinMeeting: "+"ZoomSDK has not been initialized successfully");
            return;
        }

        MeetingService meetingService = zoomSDK.getMeetingService();
        MeetingOptions opts = new MeetingOptions();
        opts.no_driving_mode = true;
		opts.no_invite = true;
        opts.no_meeting_end_message = true;
        opts.custom_meeting_id = "My Live Cricket";
        opts.no_titlebar = false;
        opts.no_bottom_toolbar = false;
        opts.no_dial_in_via_phone = true;
        opts.no_dial_out_to_phone = true;
        opts.no_disconnect_audio = true;
        opts.no_video = true;
        opts.no_share = false;
//        opts.no_titlebar = false;
        opts.meeting_views_options = MeetingViewsOptions.NO_BUTTON_PARTICIPANTS;
        opts.no_webinar_register_dialog = true;

//        int ret = meetingService.joinMeeting(this, meetingNo, randomuser, password, opts);

        JoinMeetingParams params = new JoinMeetingParams();
        params.displayName = username;
        params.meetingNo = "xxxxxxxxxx";
        params.password = "xxxxxxxxxx";

//        meetingService.joinMeeting(this, meetingNo, randomuser, password, opts);
        meetingService.joinMeeting(this, meetingNo, String.valueOf(params), opts);


    }

    @Override
    public void onMeetingStatusChanged(MeetingStatus meetingStatus, int errorCode,
                                       int internalErrorCode) {

//        Log.e(TAG, "onMeetingStatusChanged11: "+meetingStatus  + "  " +errorCode + " " +internalErrorCode);
        Log.d(TAG, "meetingStatus : "+meetingStatus);
        Log.d(TAG, "errorCode : "+errorCode);
        Log.d(TAG, "internalErrorCode : "+internalErrorCode);

        if(meetingStatus == meetingStatus.MEETING_STATUS_CONNECTING && errorCode == MeetingError.MEETING_ERROR_SUCCESS){
            Log.e(TAG, "onMeetingStatusChanged22: "+"connect" );

            progress.hideDialog();
//            Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();

//            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//            getSupportActionBar().setCustomView(R.layout.zm_meeting_toolbar);

            if (getSupportActionBar() != null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }
        if(meetingStatus == meetingStatus.MEETING_STATUS_IDLE && errorCode == MeetingError.MEETING_ERROR_SUCCESS){
            Log.e(TAG, "onMeetingStatusChanged33: ");
            progress.hideDialog();
            startActivity(new Intent(MainActivity.this, HomeActivity.class));
            finish();
        }
        if (meetingStatus == meetingStatus.MEETING_STATUS_FAILED && errorCode == MeetingError.MEETING_ERROR_CLIENT_INCOMPATIBLE) {
            Log.e(TAG, "onMeetingStatusChanged44: ");
            Log.d(TAG, "meetingStatus: " + meetingStatus);
            Log.d(TAG, "errorCode: " + errorCode);
            Toast.makeText(this, "Version of ZoomSDK is too low!", Toast.LENGTH_LONG).show();
            progress.hideDialog();
            startActivity(new Intent(MainActivity.this, HomeActivity.class));
            finish();

        }

        if (meetingStatus == MeetingStatus.MEETING_STATUS_IDLE || meetingStatus == MeetingStatus.MEETING_STATUS_FAILED) {
//            selectTab(TAB_WELCOME);
            Log.e(TAG, "onMeetingStatusChanged55: ");
            progress.hideDialog();
            startActivity(new Intent(MainActivity.this, HomeActivity.class));
            finish();
        }


    }



//    private void getRoom() {
//
//        progress.createDialog(false);
//        progress.DialogMessage(getString(R.string.please_wait));
//        progress.showDialog();
//        utils.hideKeyboard();
//
//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//        retrofit2.Call<getRoomid> responseCall = apiService.TWILIO_TOKEN_CALL(jaydeep);
//
//        final Call<getRoomid> getRoomidCall = apiService.ROOMID_CALL("sample");
//
////        Utils.Log(TAG, "GetListss lan: " + languageListCall);
//
//        getRoomidCall.enqueue(new Callback<getRoomid>() {
//            @Override
//            public void onResponse(retrofit2.Call<getRoomid> call, Response<getRoomid> response) {
//                progress.hideDialog();
//                Log.e(TAG, "onResponse: "+response);
//                final getRoomid response1 = response.body();
//                if (response.code() == 200) {
//
//                    meeting_id.setText(response1.getId());
//
//                } else {
//                    Log.e(TAG, "onResponse: " + response.code());
//                }
//            }
//
//            @Override
//            public void onFailure(retrofit2.Call<getRoomid> call, Throwable t) {
//                progress.hideDialog();
//                Log.e(TAG, "onFailure: " + t.getMessage() + " " + t.getStackTrace().toString());
//            }
//        });
//    }


  /*  private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progress.createDialog(false);
            progress.DialogMessage(getString(R.string.please_wait));
            progress.showDialog();
            utils.hideKeyboard();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall("http://45.56.125.153/ZoomDemo/createZoomMeeting.php");

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    Log.e(TAG, "doInBackground: " + jsonObj.getString("id"));
                    mettingID = jsonObj.getString("id");


                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            progress.hideDialog();

            meeting_id.setText(mettingID);
        }

    }

*/
}
