package com.livecricket.krishna.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.livecricket.krishna.R;
import com.livecricket.krishna.fragment.LogoutFragment;
import com.livecricket.krishna.fragment.UserProfileFrament;
import com.livecricket.krishna.fragment.WebinrFragment;
import com.livecricket.krishna.utils.AppConfig;
import com.livecricket.krishna.utils.PreferenceHelper;
import com.livecricket.krishna.utils.Progress;
import com.livecricket.krishna.utils.Utils;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.ZoomSDK;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "HomeActivity";

    @BindView(R.id.tb_homescreen)
    Toolbar tbHomescreen;
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;

    private Progress progress;
    private Utils utils;
    private PreferenceHelper helper;
    private String subenddate="";
    private Date date;
    private int days;
    private String username;
    private int months;
    private int hours;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(tbHomescreen);

        progress = new Progress(HomeActivity.this);
        utils = new Utils(HomeActivity.this);
        helper = new PreferenceHelper(HomeActivity.this, AppConfig.PREFERENCE.PREF_FILE);



        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigation.inflateMenu(R.menu.menu_bottom_navigation);
        bottomNavigation.getMenu().getItem(1).setChecked(true);
        setWebInr(new WebinrFragment().newInstance());

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.action_userprofile:
                        setUserProfile(new UserProfileFrament().newInstance());
                        break;
                    case R.id.action_webinrlist:
                        setWebInr(new WebinrFragment().newInstance());
                        break;
                    case R.id.action_logout:
                        setLogout(new LogoutFragment().newInstance());
                        break;
                }
                return true;
            }
        });


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        startActivity(new Intent(HomeActivity.this, HomeActivity.class));
        finish();
    }

    private void logout() {

        AlertDialog.Builder builderInner = new AlertDialog.Builder(this);
        builderInner.setTitle(getResources().getString(R.string.title_logoutdialog));
        builderInner.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                helper.clearAllPrefs();
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                finish();
            }
        });
        builderInner.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        builderInner.show();

    }

    private void setUserProfile(UserProfileFrament userProfileFrament) {
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        t.replace(R.id.main_container, userProfileFrament);
        t.commitAllowingStateLoss();
    }

    private void setWebInr(WebinrFragment webinrFragment) {
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        t.replace(R.id.main_container, webinrFragment);
        t.commitAllowingStateLoss();
    }

    private void setLogout(LogoutFragment logoutFragment) {
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        t.replace(R.id.main_container, logoutFragment);
        t.commitAllowingStateLoss();
    }




}
