package com.livecricket.krishna.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.livecricket.krishna.R;
import com.livecricket.krishna.network.ApiClient;
import com.livecricket.krishna.network.ApiInterface;
import com.livecricket.krishna.responce.CommonResponce;
import com.livecricket.krishna.utils.AppConfig;
import com.livecricket.krishna.utils.PreferenceHelper;
import com.livecricket.krishna.utils.Progress;
import com.livecricket.krishna.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.tb_signup)
    Toolbar tbSignup;
    @BindView(R.id.et_lname)
    EditText etLname;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_conpassword)
    EditText etConpassword;
    @BindView(R.id.btn_signup)
    Button btnSignup;
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @BindView(R.id.txt_signin)
    TextView txtSignin;
    @BindView(R.id.et_username)
    EditText etUsername;
    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;

    private static final String TAG = "RegisterActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        setSupportActionBar(tbSignup);

        utils = new Utils(this);
        progress = new Progress(this);
        helper = new PreferenceHelper(this, AppConfig.PREFERENCE.PREF_FILE);

    }


    private void signup() {


        String fullname = etLname.getText().toString();
        String email = etEmail.getText().toString();
        String passwrod = etPassword.getText().toString();
        String confirmpwd = etConpassword.getText().toString();
        String phone = etMobile.getText().toString();
        String username = etUsername.getText().toString();


        if (fullname.isEmpty()) {

            etLname.setError("Enter the fullname");
            utils.requestFocus(etLname);
            return;
        }
//        if (email.isEmpty()) {
//
//            etEmail.setError("Enter the email");
//            utils.requestFocus(etEmail);
//            return;
//        }
//        if (phone.isEmpty()) {
//
//            etMobile.setError("Enter the Mobile No");
//            utils.requestFocus(etMobile);
//            return;
//        }
        if (passwrod.isEmpty()) {

            etPassword.setError("Enter the Password");
            utils.requestFocus(etPassword);
            return;
        }
        if (confirmpwd.isEmpty()) {

            etConpassword.setError("Enter the Confirm Password");
            utils.requestFocus(etConpassword);
            return;
        }
        if (username.isEmpty()) {

            etUsername.setError("Enter the Username");
            utils.requestFocus(etUsername);
            return;
        }

//        if (!validateuserId()) {
//            return;
//        }

        if (!passwrod.equalsIgnoreCase(confirmpwd)) {
            Toast.makeText(this, "Password are not matches", Toast.LENGTH_SHORT).show();
            return;
        }

        getSignup(fullname, phone, email, passwrod, username);


    }

    private boolean validateuserId() {
        String email = etEmail.getText().toString().trim();
        if (email.isEmpty() || !Utils.isValidEmail(email)) {
            etEmail.setError("Please Enter Valid Email ID");
            utils.requestFocus(etEmail);
            return false;
        } else {
            return true;
        }
    }

    @OnClick({R.id.btn_signup, R.id.txt_signin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:
                signup();
                break;
            case R.id.txt_signin:
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
                break;
        }
    }

    private void getSignup(String fullname, String phoneno, String email, String passwrod, String username) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();
        utils.hideKeyboard();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonResponce> signUPResponceCall = apiService.COMMON_RESPONCE_CALL(fullname, passwrod, username);

        signUPResponceCall.enqueue(new Callback<CommonResponce>() {
            @Override
            public void onResponse(Call<CommonResponce> call, Response<CommonResponce> response) {
                progress.hideDialog();
                final CommonResponce commonResponce = response.body();
                if (response.code() == AppConfig.URL.SUCCESS) {
                    if (!response.body().isError()) {

                        Toast.makeText(RegisterActivity.this, commonResponce.getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();


                    } else {
                        Toast.makeText(RegisterActivity.this, commonResponce.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(RegisterActivity.this, getString(R.string.msg_unexpected_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponce> call, Throwable t) {
                progress.hideDialog();
                Log.e(TAG, "onFailure: "+t.getMessage() );
                Log.e(TAG, "onFailure: "+ t.getCause());
                t.printStackTrace();
                Toast.makeText(RegisterActivity.this, getString(R.string.msg_internet_conn), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
