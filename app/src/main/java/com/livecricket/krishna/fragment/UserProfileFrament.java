package com.livecricket.krishna.fragment;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.livecricket.krishna.R;
import com.livecricket.krishna.network.ApiClient;
import com.livecricket.krishna.network.ApiInterface;
import com.livecricket.krishna.responce.CommonResponce;
import com.livecricket.krishna.utils.AppConfig;
import com.livecricket.krishna.utils.PreferenceHelper;
import com.livecricket.krishna.utils.Progress;
import com.livecricket.krishna.utils.Utils;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileFrament extends Fragment {

    private static final String TAG = "UserProfileFrament";

    Unbinder unbinder;
    @BindView(R.id.et_userlname)
    EditText etLname;
    @BindView(R.id.et_useremail)
    EditText etEmail;
    @BindView(R.id.et_usermobile)
    EditText etMobile;
    @BindView(R.id.txt_changepwd)
    TextView txtChangepwd;
    @BindView(R.id.btn_signin)
    Button btnSignin;
    @BindView(R.id.et_username)
    EditText etUsername;

    private Dialog changepwdDialog;
    private EditText etcurrentpwd, etNewpwd, etchnagepwd;
    private TextView txtCancel;
    private Button btnchangepwd;

    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;
    private String apikey = "";
    private String subenddate;
    private String username;
    private Date date;
    private int hours;
    private int days;


    public UserProfileFrament() {
        // Required empty public constructor
    }

    public static UserProfileFrament newInstance() {

        Bundle args = new Bundle();

        UserProfileFrament fragment = new UserProfileFrament();
        fragment.setArguments(args);
        return fragment;
    }

    // TODO: Rename and change types and number of parameters

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile_frament, container, false);
        unbinder = ButterKnife.bind(this, view);

        utils = new Utils(getActivity());
        progress = new Progress(getActivity());
        helper = new PreferenceHelper(getActivity(), AppConfig.PREFERENCE.PREF_FILE);

        etLname.setText(helper.LoadStringPref(AppConfig.PREFERENCE.FULL_NAME, ""));
        etMobile.setText(helper.LoadStringPref(AppConfig.PREFERENCE.USER_NO, ""));
        etEmail.setText(helper.LoadStringPref(AppConfig.PREFERENCE.USER_EMAIL, ""));
        etUsername.setText(helper.LoadStringPref(AppConfig.PREFERENCE.USER_NAME, ""));

        apikey = helper.LoadStringPref(AppConfig.PREFERENCE.USER_AUTHKEY, "");


//        subenddate = helper.LoadStringPref(AppConfig.PREFERENCE.SUBEND_DATE, "");
//        username = helper.LoadStringPref(AppConfig.PREFERENCE.FULL_NAME, "");
//
//        Log.e(TAG, "onCreate: " + subenddate);
//
//
//        try {
//            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//            date = (Date) formatter.parse(subenddate);
//            long currenttimestamp = new Date().getTime();
//
//
//            long difference = currenttimestamp - date.getTime();
//            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
//            days = hours / 24;
//            String totaldays = String.valueOf(days);
//
//            String subtrace = totaldays.substring(1);
//
//
//            txtEndsub.setText("Hi "+ username + ". You have " + subtrace +" days of Subscription Left");
////
//
//        } catch (ParseException e) {
//
//
//            e.printStackTrace();
//        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.txt_changepwd, R.id.btn_signin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_changepwd:
                openChangePWd();
                break;
            case R.id.btn_signin:
                update();
                break;
        }
    }

    private void update() {

        String fullname = etLname.getText().toString();
        String number = etMobile.getText().toString();
        if (fullname.isEmpty()) {

            etLname.setError("Enter the fullname");
            utils.requestFocus(etLname);
            return;
        }
        if (number.isEmpty()) {

            etMobile.setError("Enter the Mobile");
            utils.requestFocus(etMobile);
            return;
        }

        updateProfile(apikey, fullname, number);
    }

    private void openChangePWd() {

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_changepwd, null);

        changepwdDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        changepwdDialog.setContentView(view);
        changepwdDialog.setCancelable(true);
        changepwdDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        changepwdDialog.getWindow().setGravity(Gravity.CENTER);

        etcurrentpwd = (EditText) view.findViewById(R.id.et_currentpwd);
        etNewpwd = (EditText) view.findViewById(R.id.et_changepwd);
        etchnagepwd = (EditText) view.findViewById(R.id.et_changeconpwd);
        txtCancel = (TextView) view.findViewById(R.id.txt_cancel);
        btnchangepwd = (Button) view.findViewById(R.id.btn_changepwd);


        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changepwdDialog.dismiss();
            }
        });

        btnchangepwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                passWord();
            }
        });


        changepwdDialog.show();

    }

    private void passWord() {

        String currentpwd = etcurrentpwd.getText().toString();
        String newpassword = etNewpwd.getText().toString();
        String changepassword = etchnagepwd.getText().toString();

        if (currentpwd.isEmpty()) {

            etcurrentpwd.setError("Enter the current password");
            utils.requestFocus(etcurrentpwd);
            return;
        }
        if (newpassword.isEmpty()) {

            etNewpwd.setError("Enter the new password");
            utils.requestFocus(etNewpwd);
            return;
        }
        if (changepassword.isEmpty()) {

            etchnagepwd.setError("Enter the change password");
            utils.requestFocus(etchnagepwd);
            return;
        }
        if (!newpassword.equalsIgnoreCase(changepassword)) {
            Toast.makeText(getActivity(), "Password are not matches", Toast.LENGTH_SHORT).show();
            return;
        }

        changePwd(apikey, currentpwd, newpassword);

    }

    private void updateProfile(String apikey, final String name, final String contactNo) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();
        utils.hideKeyboard();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonResponce> updateUser = apiService.UPDATE_USER(apikey, name, contactNo);

        updateUser.enqueue(new Callback<CommonResponce>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponce> call, @NonNull Response<CommonResponce> response) {
                progress.hideDialog();
                CommonResponce CommonResponce = response.body();

                if (response.code() == AppConfig.URL.SUCCESS) {
                    if (!response.body().isError()) {

                        helper.initPref();
                        helper.SaveStringPref(AppConfig.PREFERENCE.FULL_NAME, name);
                        helper.SaveStringPref(AppConfig.PREFERENCE.USER_NO, contactNo);
                        helper.ApplyPref();

                        Toast.makeText(getActivity(), "Update Profile Successful", Toast.LENGTH_SHORT).show();

                    } else {

                        Log.e(TAG, "onResponse: " + response.message());
                        Toast.makeText(getActivity(), CommonResponce.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.msg_unexpected_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponce> call, @NonNull Throwable t) {
                progress.hideDialog();

                Toast.makeText(getActivity(), getString(R.string.msg_internet_conn), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void changePwd(String apikey, String currentpwd, String newpwd) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();
        utils.hideKeyboard();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonResponce> updateUser = apiService.CHANGE_PWD(apikey, currentpwd, newpwd);

        updateUser.enqueue(new Callback<CommonResponce>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponce> call, @NonNull Response<CommonResponce> response) {
                progress.hideDialog();
                CommonResponce CommonResponce = response.body();

                if (response.code() == AppConfig.URL.SUCCESS) {
                    if (!response.body().isError()) {
                        changepwdDialog.dismiss();
                        Toast.makeText(getActivity(), "Change Password Successful", Toast.LENGTH_SHORT).show();

                    } else {

                        Log.e(TAG, "onResponse: " + response.message());
                        Toast.makeText(getActivity(), CommonResponce.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.msg_unexpected_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponce> call, @NonNull Throwable t) {
                progress.hideDialog();
                Toast.makeText(getActivity(), getString(R.string.msg_internet_conn), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
