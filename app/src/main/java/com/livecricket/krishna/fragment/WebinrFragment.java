package com.livecricket.krishna.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.livecricket.krishna.R;
import com.livecricket.krishna.activity.LoginActivity;
import com.livecricket.krishna.adapter.EventAdapter;
import com.livecricket.krishna.network.ApiClient;
import com.livecricket.krishna.network.ApiInterface;
import com.livecricket.krishna.responce.WebInrResponce;
import com.livecricket.krishna.utils.AppConfig;
import com.livecricket.krishna.utils.PreferenceHelper;
import com.livecricket.krishna.utils.Progress;
import com.livecricket.krishna.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WebinrFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "WebinrFragment";
    Unbinder unbinder;
    @BindView(R.id.rv_webinrlist)
    RecyclerView rvWebinrlist;
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.txt_endsub)
    TextView txtEndsub;

    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;

    private EventAdapter adapter;
    private String android_id = "";

    private String subenddate;
    private String username;
    private Date date;
    private int hours;
    private int days;
    private Activity mActivity;
    private int check = 0;


    public WebinrFragment() {
        // Required empty public constructor
    }

    public static WebinrFragment newInstance() {

        Bundle args = new Bundle();

        WebinrFragment fragment = new WebinrFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_webinr, container, false);
        unbinder = ButterKnife.bind(this, view);

        utils = new Utils(getActivity());
        progress = new Progress(getActivity());
        helper = new PreferenceHelper(getActivity(), AppConfig.PREFERENCE.PREF_FILE);


//        android_id = Settings.Secure.getString(getActivity().getContentResolver(),
//                Settings.Secure.ANDROID_ID);

//        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvWebinrlist.setHasFixedSize(true);//every item of the RecyclerView has a fix size
        rvWebinrlist.setLayoutManager(new LinearLayoutManager(getContext()));

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

//        if(check==1){
//
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    /* Create an Intent that will start the Menu-Activity. */
//                    check=0;
//                    txtEndsub.setVisibility(View.GONE);
//
//                }
//            }, 5000);
//
//
//
//        }
        disSubend();


        Log.e(TAG, "onCreateView: " + helper.LoadStringPref(AppConfig.PREFERENCE.USER_AUTHKEY, ""));

        webinrList(helper.LoadStringPref(AppConfig.PREFERENCE.USER_AUTHKEY, ""));



        return view;
    }

    private void disSubend() {

        subenddate = helper.LoadStringPref(AppConfig.PREFERENCE.SUBEND_DATE, "");
        username = helper.LoadStringPref(AppConfig.PREFERENCE.FULL_NAME, "");

        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            date = (Date) formatter.parse(subenddate);
            long currenttimestamp = new Date().getTime();


            long difference = currenttimestamp - date.getTime();
            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            days = hours / 24;
            String totaldays = String.valueOf(days);
            String subtrace = totaldays.substring(1);

            txtEndsub.setText("Your subscription will expire in another " + subtrace + " days.Please contact admin to renew");
//

        } catch (ParseException e) {


            e.printStackTrace();
        }

    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void webinrList(String apikey) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();
        utils.hideKeyboard();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<WebInrResponce> webInrResponceCall = apiService.WEB_INR_RESPONCE_CALL(apikey, "1");

        webInrResponceCall.enqueue(new Callback<WebInrResponce>() {
            @Override
            public void onResponse(@NonNull Call<WebInrResponce> call, @NonNull Response<WebInrResponce> response) {
                progress.hideDialog();
                mSwipeRefreshLayout.setRefreshing(false);
                WebInrResponce webInrResponce = response.body();

                if (response.code() == AppConfig.URL.SUCCESS) {

                    if (!response.body().isError()) {


                        Log.e(TAG, "onResponse: " + webInrResponce.getStatus());
                        Log.e(TAG, "webInrResponce getStatus: " + webInrResponce.getStatus());
                        Log.e(TAG, "webInrResponce msg : " + webInrResponce.getMessage());

                        if (webInrResponce.getStatus().equalsIgnoreCase("0")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.msg_block), Toast.LENGTH_SHORT).show();
                            helper.clearAllPrefs();
                            startActivity(new Intent(getActivity(), LoginActivity.class));
                            getActivity().finish();
                            return;
                        }
//                        Log.e(TAG, "onResponse: " + android_id + "  " + webInrResponce.getDeviceid());
//                        if (!android_id.equalsIgnoreCase(webInrResponce.getDeviceid())) {
//                            Toast.makeText(getActivity(), getResources().getString(R.string.msg_deviceid), Toast.LENGTH_SHORT).show();
//                            helper.clearAllPrefs();
//                            startActivity(new Intent(getActivity(), LoginActivity.class));
//                            getActivity().finish();
//                            return;
//
//                        }
                        helper.initPref();
                        helper.SaveStringPref(AppConfig.PREFERENCE.ZOOMKEY, webInrResponce.getAppKey());
                        helper.SaveStringPref(AppConfig.PREFERENCE.ZOOMSECRET, webInrResponce.getSecKey());
                        helper.SaveStringPref(AppConfig.PREFERENCE.RANDOMUSERSTRING, webInrResponce.getRandstring());
                        helper.ApplyPref();

                        adapter = new EventAdapter(getActivity(), webInrResponce.getWebdetails());
                        rvWebinrlist.setAdapter(adapter);


                    } else {

                        Log.e(TAG, "onResponseeee: " + webInrResponce.getStatus());


                        if (webInrResponce.getStatus().equalsIgnoreCase("0")) {

                            Log.e(TAG, "onResponse: " + webInrResponce.getStatus());

                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                            getActivity().finish();

//                            startActivity(new Intent(getActivity(), LoginActivity.class));
//                            getActivity().finish();
//                            return;
                        }
                        Log.e(TAG, "onResponse: " + response.message());
                        Log.d(TAG, "webInrResponce: " + webInrResponce.getMessage());
                        Toast.makeText(getActivity(), webInrResponce.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(getActivity(), getString(R.string.msg_unexpected_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<WebInrResponce> call, @NonNull Throwable t) {
                progress.hideDialog();
                mSwipeRefreshLayout.setRefreshing(false);
                Log.e(TAG, "onFailure: " + t.getMessage());
                Log.e(TAG, "onFailure: " + t.getCause());
                Log.e(TAG, "onFailure: " + t.getStackTrace());
                Toast.makeText(getActivity(), getString(R.string.msg_internet_conn), Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public void onRefresh() {

        webinrList(helper.LoadStringPref(AppConfig.PREFERENCE.USER_AUTHKEY, ""));

    }
}
