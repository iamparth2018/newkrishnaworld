package com.livecricket.krishna.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.livecricket.krishna.R;
import com.livecricket.krishna.activity.LoginActivity;
import com.livecricket.krishna.network.ApiClient;
import com.livecricket.krishna.network.ApiInterface;
import com.livecricket.krishna.responce.LogoutResponce;
import com.livecricket.krishna.utils.AppConfig;
import com.livecricket.krishna.utils.PreferenceHelper;
import com.livecricket.krishna.utils.Progress;
import com.livecricket.krishna.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LogoutFragment extends Fragment {

    private static final String TAG = "LogoutFragment";

    @BindView(R.id.btn_logout)
    Button btnLogout;
    Unbinder unbinder;
    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;

    public static LogoutFragment newInstance() {

        Bundle args = new Bundle();

        LogoutFragment fragment = new LogoutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public LogoutFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_logout, container, false);
        unbinder = ButterKnife.bind(this, view);
        utils = new Utils(getActivity());
        progress = new Progress(getActivity());
        helper = new PreferenceHelper(getActivity(), AppConfig.PREFERENCE.PREF_FILE);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_logout)
    public void onViewClicked() {

        logout();

    }

    private void logout() {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();
        utils.hideKeyboard();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<LogoutResponce> logoutResponceCall = apiService.LOGOUT_RESPONCE_CALL(helper.LoadStringPref(AppConfig.PREFERENCE.USER_AUTHKEY, ""),"0");

        logoutResponceCall.enqueue(new Callback<LogoutResponce>() {
            @Override
            public void onResponse(@NonNull Call<LogoutResponce> call, @NonNull Response<LogoutResponce> response) {
                progress.hideDialog();
                LogoutResponce logoutResponce = response.body();


                if (response.code() == AppConfig.URL.SUCCESS) {
                    if (!response.body().isError()) {

                        helper.clearAllPrefs();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();

                    } else {

                        Log.e(TAG, "onResponse: "+response.message() );
                        Toast.makeText(getActivity(), logoutResponce.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.msg_unexpected_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<LogoutResponce> call, @NonNull Throwable t) {
                progress.hideDialog();
                Log.e(TAG, "onFailure: "+t.getMessage() );
                Log.e(TAG, "onFailure: "+t.getCause() );
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.msg_internet_conn), Toast.LENGTH_SHORT).show();            }
        });

    }
}
