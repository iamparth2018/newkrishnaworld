package com.livecricket.krishna.adapter;

import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.livecricket.krishna.R;
import com.livecricket.krishna.activity.MainActivity;
import com.livecricket.krishna.responce.WebInrResponce;
import com.livecricket.krishna.utils.AppConfig;
import com.livecricket.krishna.utils.PreferenceHelper;
import com.livecricket.krishna.utils.Progress;
import com.livecricket.krishna.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by android2 on 9/28/17.
 */


public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {

    private static final String TAG = "EventAdapter";

    private List<WebInrResponce.WebdetailsEntity> lists;
    private FragmentActivity mContext;

    private PreferenceHelper helper;
    private Progress progress;
    private Utils utils;
    private String str_date;
    private String end_date;
    private Date date;
    private Date date2;
    private int hours;
    private int days;
    private int min;


    public EventAdapter(FragmentActivity activity, List<WebInrResponce.WebdetailsEntity> lists) {
        this.lists = lists;
        this.mContext = activity;
        helper = new PreferenceHelper(mContext, AppConfig.PREFERENCE.PREF_FILE);
        progress = new Progress(mContext);
        utils = new Utils(mContext);

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_webinrlist, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final WebInrResponce.WebdetailsEntity entity = lists.get(position);

        date = null;
        date2 = null;
        try {
            str_date = entity.getFromDate();
            end_date = entity.getEndDate();
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm");
            date = (Date) formatter.parse(str_date);
            date2 = (Date) formatter.parse(end_date);

            long currenttimestamp = new Date().getTime();
            Log.e(TAG, "onClick: " + date.getTime() + "   --  " + date2.getTime() + "  -- " + currenttimestamp);

            long difference = date2.getTime() - date.getTime();
            days = (int) (difference / (1000 * 60 * 60 * 24));
            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            hours = (hours < 0 ? -hours : hours);
            Log.e("======= Hours", " :: " + hours);


            Log.e(TAG, "onBindViewHolder: " + new Date().getTime() + "  " + date.getTime());
            long difference2 = date.getTime() - new Date().getTime();
            Log.e(TAG, "onBindViewHolder: " + difference2);
            CountDownTimer timer = new CountDownTimer(difference2, 1000) {
                public void onTick(long millisUntilFinished) {
                    int seconds = (int) (millisUntilFinished / 1000) % 60;
                    int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                    int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);
                    int day = (int) (millisUntilFinished / (1000 * 60 * 60 * 24));
//                    holder.txtrmaintime.setText(String.format("%d:%d:%d:%d",day +" days",hours + " hrs",minutes + " min",seconds + " sec"));
                    holder.txtrmaintime.setText(day + "days:" + hours + "hrs:" + minutes + "min:" + seconds + "sec");
                }

                public void onFinish() {
                    holder.txtrmaintime.setText("");
                }
            };
            timer.start();

            if (currenttimestamp >= date.getTime() && currenttimestamp <= date2.getTime()) {

                holder.btnjoin.setBackgroundResource(R.drawable.btnnn_background);

            } else {

                holder.btnjoin.setBackgroundResource(R.drawable.btnnn_graybackground);
//                Toast.makeText(mContext, "You did not join meeting before starttime", Toast.LENGTH_SHORT).show();
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.txtmeetingname.setText(entity.getWebinarName());

        String startdate = entity.getFromDate();
        String enddate = entity.getEndDate();
//        int i = startdate.indexOf(" ");
//        startdate = startdate.substring(0,i);
        String[] parts = startdate.split(" ");
        String sdate = parts[0];//"hello"
        String starttime = parts[1];//"World"

        String[] parts2 = enddate.split(" ");
        final String edate = parts2[0];//"hello"
        String endtime = parts2[1];//"World"

        holder.txtdate.setText("Date: " + sdate);
        holder.txtstime.setText("Start Time: " + starttime);
        holder.txtendtime.setText("End Time: " + endtime);
        if (hours == 0) {
            holder.txtduration.setText("Duration: " + min + " min");

        } else if (hours == 1) {

            holder.txtduration.setText("Duration: " + hours + " hrs");

        } else if (hours >= 1) {
            holder.txtduration.setText("Duration: " + hours + " hrs " + min + " min");
        } else if (hours >= 24) {
            holder.txtduration.setText("Duration: " + days + " days");
        }


        holder.btnjoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                date = null;
                date2 = null;
                try {
                    str_date = entity.getFromDate();
                    end_date = entity.getEndDate();
                    DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm");
                    date = (Date) formatter.parse(str_date);
                    date2 = (Date) formatter.parse(end_date);

                    long currenttimestamp = new Date().getTime();
                    Log.e(TAG, "onClick: " + date.getTime() + "   --  " + date2.getTime() + "  -- " + currenttimestamp);

                    if (currenttimestamp >= date.getTime() && currenttimestamp <= date2.getTime()) {

                        helper.initPref();
                        helper.SaveStringPref(AppConfig.PREFERENCE.MEETINGID, entity.getMeetingId());
                        helper.SaveStringPref(AppConfig.PREFERENCE.MEETINGPWD, entity.getPassword());
                        helper.ApplyPref();
                        mContext.startActivity(new Intent(mContext, MainActivity.class));
                        mContext.finish();


                    } else {

                        Toast.makeText(mContext, "You can not join meeting before start time", Toast.LENGTH_SHORT).show();

                    }


                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtmeetingname, txtdate, txtstime, txtendtime, txtduration, txtrmaintime;
        public Button btnjoin;
        public LinearLayout lay_itemview;


        public MyViewHolder(View view) {
            super(view);

            txtmeetingname = (TextView) view.findViewById(R.id.txt_meetingname);
            txtdate = (TextView) view.findViewById(R.id.txt_date);
            txtstime = (TextView) view.findViewById(R.id.txt_starttime);
            txtendtime = (TextView) view.findViewById(R.id.txt_endtime);
            txtrmaintime = (TextView) view.findViewById(R.id.txt_timer);
            txtduration = (TextView) view.findViewById(R.id.txt_duration);
            btnjoin = (Button) view.findViewById(R.id.btn_join);
            lay_itemview = (LinearLayout) view.findViewById(R.id.lay_itemview);

        }

    }


}