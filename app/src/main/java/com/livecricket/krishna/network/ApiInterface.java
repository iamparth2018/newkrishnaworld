package com.livecricket.krishna.network;


import com.livecricket.krishna.responce.CommonResponce;
import com.livecricket.krishna.responce.LoginResonce;
import com.livecricket.krishna.responce.LogoutResponce;
import com.livecricket.krishna.responce.WebInrResponce;
import com.livecricket.krishna.utils.AppConfig;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface {


    //    @FormUrlEncoded
//    @POST(AppConfig.URL.Login)
//    Call<ResponseBody> LOGIN_RESPONCE_CALLl(@Field("email") String email, @Field("password") String password);
//
    @FormUrlEncoded
    @POST(AppConfig.URL.Login)
    Call<LoginResonce> LOGIN_RESPONCE_CALL(@Field("uname") String email, @Field("password") String password,
                                           @Field("deviceid") String deviceid);

    @FormUrlEncoded
    @POST(AppConfig.URL.Register)
    Call<CommonResponce> COMMON_RESPONCE_CALL(@Field("name") String name,  @Field("password") String password,
                                              @Field("uname") String username);
    @FormUrlEncoded
    @POST(AppConfig.URL.Webinrlist)
    Call<WebInrResponce> WEB_INR_RESPONCE_CALL(@Header("Header") String apikey, @Field("in_out") String inout);

    @FormUrlEncoded
    @POST(AppConfig.URL.UpdateUser)
    Call<CommonResponce> UPDATE_USER(@Header("Header") String apikey, @Field("name") String name,
                                     @Field("contactNumber") String contactNo);

    @FormUrlEncoded
    @POST(AppConfig.URL.ChangePwd)
    Call<CommonResponce> CHANGE_PWD(@Header("Header") String apikey, @Field("current_password") String name,
                                    @Field("new_password") String contactNo);

    @FormUrlEncoded
    @POST(AppConfig.URL.Logout)
    Call<LogoutResponce> LOGOUT_RESPONCE_CALL(@Header("Header") String apikey,
                                    @Field("in_out") String inout);


}
