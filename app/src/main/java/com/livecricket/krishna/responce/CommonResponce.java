package com.livecricket.krishna.responce;

import com.google.gson.annotations.SerializedName;

public class CommonResponce {


    /**
     * error : false
     * message : success
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
